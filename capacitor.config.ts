import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.terry.login',
  appName: 'Login',
  webDir: 'build',
  bundledWebRuntime: false
};

export default config;
