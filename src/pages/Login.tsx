import React, { useState, useEffect } from 'react'
import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
  IonRow,
  IonCol,
  IonItem,
  IonLabel,
  IonInput,
  IonButton,
} from '@ionic/react'
import { useHistory } from 'react-router-dom'
import { Database, Storage } from '@ionic/storage'
import { FingerprintAIO } from '@ionic-native/fingerprint-aio'
import { TouchID } from '@ionic-native/touch-id'
import * as CryptoJS from 'crypto-js'
import LoginForm from '../components/LoginForm'
import { useAuth } from '../hooks/useAuth'

const Login: React.FC = () => {
  const [db, setDb] = useState<Database | null>(null)
  const [credentials, setCredentials] = useState<string>()
  const history = useHistory()
  const { data, loading, error, authenticate } = useAuth()

  useEffect(() => {
    async function initDb() {
      const store = new Storage()

      const db = await store.create()

      setDb(db)
    }

    initDb()
  }, [])

  useEffect(() => {
    async function getData() {
      const val = await db.get('credentials')
      // db.remove('credentials')
      // alert(val)
      setCredentials(val)
    }

    if (db) {
      // db.set('credentials', null)
      getData()
    }
  }, [db])

  useEffect(() => {
    if (data) {
      history.replace('/tab1')
    }
  }, [data])

  const openScanner = async () => {
    const ciphertext = await db.get('credentials')
    if (!ciphertext) {
      history.push('/login/touch-id')
      return
    }
    //Este es para android
    // try {
    //   await FingerprintAIO.isAvailable()
    //   const res = await FingerprintAIO.show({})
    //   alert(res)
    // } catch (err) {
    //   alert(JSON.stringify(err))
    // }

    //Iphon
    try {
      await TouchID.isAvailable()
      // alert(available)
      await TouchID.verifyFingerprintWithCustomPasswordFallback(
        'Ponga el dedito'
      )
      // alert(result)
      const bytes = CryptoJS.AES.decrypt(ciphertext, 'my-secret-key@123')
      const data = JSON.parse(bytes.toString(CryptoJS.enc.Utf8))
      // alert('Bien pueda!' + JSON.stringify(data))
      authenticate({ data })
    } catch (err) {
      alert(JSON.stringify(err))
    }
  }

  const handleAuth = (data: any) => {
    // alert(JSON.stringify(data))
    authenticate({ data })
    // history.replace('/tab1')
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Login</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen className="ion-padding ion-text-center">
        <LoginForm
          name="lol"
          loading={loading}
          showFinger
          onFingerClick={openScanner}
          onAuth={handleAuth}
          showMessage={data === null}
          fingerMessage={
            credentials ? 'Ingresacon con TouchID' : 'Registrar TouchID'
          }
        />
      </IonContent>
    </IonPage>
  )
}

export default Login
