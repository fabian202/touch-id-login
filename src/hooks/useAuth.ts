import { useState, useEffect } from 'react';
import useAxios from 'axios-hooks';

export const useAuth = () => {

  const [{ data, loading, error }, authenticate] = useAxios(
    {
      url: 'https://apidevtest.envioslatinoscorp.com/api/Authentication',
      method: 'POST',
    },
    { manual: true }
  )

  return {
    data,
    loading,
    error,
    authenticate,
  };
};
