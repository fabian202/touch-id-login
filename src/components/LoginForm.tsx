import React, { useState, useEffect } from 'react'
import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
  IonRow,
  IonCol,
  IonItem,
  IonLabel,
  IonInput,
  IonButton,
  IonLoading,
} from '@ionic/react'
import useAxios from 'axios-hooks'

interface ContainerProps {
  name: string
  showFinger?: boolean
  showMessage?: boolean
  loading: boolean
  onAuth: (data: { Name: string; Pass: string; LogDate: string }) => void
  onFingerClick?: () => void
  fingerMessage?: string
}

const LoginForm: React.FC<ContainerProps> = ({
  name,
  showFinger,
  onAuth,
  onFingerClick,
  showMessage,
  loading,
  fingerMessage,
}) => {
  const [email, setEmail] = useState<string>('JOHNT20@HOTMAIL.COM')
  const [password, setPassword] = useState<string>('A12345')

  // const [{ data: dataResponse, loading, error }, authenticate] = useAxios(
  //   {
  //     url: 'https://apidevtest.envioslatinoscorp.com/api/Authentication',
  //     method: 'POST',
  //   },
  //   { manual: true }
  // )

  // useEffect(() => {
  //   if (dataResponse) {
  //     onAuth && onAuth()
  //   }
  // }, [dataResponse])

  const handleLogin = (e: React.FormEvent) => {
    e.preventDefault()

    const yourDate = new Date()

    const data = {
      Name: email,
      Pass: password,
      LogDate: yourDate.toISOString().split('T')[0],
    }

    // authenticate({ data })
    onAuth && onAuth(data)

    // Encrypt
    // const ciphertext = CryptoJS.AES.encrypt(
    //   JSON.stringify(data),
    //   'my-secret-key@123'
    // ).toString()

    // db.set('credentials', ciphertext)
    // alert('added' + ciphertext)
    // history.replace('/tab1')
    // history.push('/tab1/')
  }

  return (
    <form onSubmit={handleLogin}>
      <IonRow>
        <IonCol>
          <IonItem>
            <IonLabel position="floating"> Email</IonLabel>
            <IonInput
              type="email"
              value={email}
              onIonChange={(e) => setEmail(e.detail.value!)}
              required
            ></IonInput>
          </IonItem>
        </IonCol>
      </IonRow>

      <IonRow>
        <IonCol>
          <IonItem>
            <IonLabel position="floating"> Password</IonLabel>
            <IonInput
              type="password"
              value={password}
              onIonChange={(e) => setPassword(e.detail.value!)}
              required
            ></IonInput>
          </IonItem>
        </IonCol>
      </IonRow>

      {showMessage && (
        <IonRow>
          <IonCol>
            <IonLabel color="danger">Email y/o Contraseña no válidos</IonLabel>
            <br />
          </IonCol>
        </IonRow>
      )}

      <IonRow>
        <IonCol>
          <IonButton type="submit" expand="block">
            {!showFinger ? 'Guardar' : 'Login'}
          </IonButton>
        </IonCol>
      </IonRow>

      {showFinger && (
        <IonRow>
          <IonCol>
            <IonButton type="button" expand="block" onClick={onFingerClick}>
              {fingerMessage}
            </IonButton>
          </IonCol>
        </IonRow>
      )}

      <IonLoading
        cssClass="my-custom-class"
        isOpen={loading}
        message={'Please wait...'}
      />
    </form>
  )
}

export default LoginForm
