import React, { useEffect, useState } from 'react'
import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
  IonButtons,
  IonBackButton,
} from '@ionic/react'
import LoginForm from '../components/LoginForm'
import { Database, Storage } from '@ionic/storage'
import { useHistory } from 'react-router-dom'
import { useAuth } from '../hooks/useAuth'
import * as CryptoJS from 'crypto-js'

const TouchID: React.FC = () => {
  const [db, setDb] = useState<Database | null>(null)
  const [credentials, setCredentials] = useState<any>(null)
  const history = useHistory()
  const { data, loading, error, authenticate } = useAuth()

  useEffect(() => {
    async function initDb() {
      const store = new Storage()

      const db = await store.create()

      setDb(db)
    }

    initDb()
  }, [])

  useEffect(() => {
    async function getData() {
      const val = await db.get('credentials')
      // alert('tiene dedito en la otra')
    }

    if (db) {
      getData()
    }
  }, [db])

  useEffect(() => {
    if (data) {
      //Save credentials
      // Encrypt
      const ciphertext = CryptoJS.AES.encrypt(
        JSON.stringify(credentials),
        'my-secret-key@123'
      ).toString()

      db.set('credentials', ciphertext)

      history.replace('/tab1')
    }
  }, [data])

  const handleAuth = (data: any) => {
    // alert(JSON.stringify(data))
    setCredentials(data)
    authenticate({ data })
    // history.replace('/tab1')
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton />
          </IonButtons>
          <IonTitle>Credentials</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen className="ion-padding ion-text-center">
        <LoginForm name="golis" loading={loading} onAuth={handleAuth} showMessage={data === null} />
      </IonContent>
    </IonPage>
  )
}

export default TouchID
